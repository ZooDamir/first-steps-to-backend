from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
import json

POSTGRES_URL = "127.0.0.1:5432"
POSTGRES_USER = "postgres"
POSTGRES_PW = "Zoopark"
POSTGRES_DB = "First"

DB_URL = 'postgresql://{user}:{pw}@{url}/{db}'.format(user=POSTGRES_USER, pw=POSTGRES_PW,
                                                      url=POSTGRES_URL, db=POSTGRES_DB)

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = DB_URL
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)


class Person(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(15))
    surname = db.Column(db.String(20))

    def __init__(self, name, surname):
        self.name = name
        self.surname = surname


class PersonEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Person):
            return {'name': obj.name, 'surname': obj.surname}
        return json.JSONEncoder.default(self, obj)


@app.route('/', methods=['GET'])
def get_person_from_db():
    person_id = request.args.get('id')

    persons = db.session.query(Person).filter_by(id=person_id).all()

    return json.dumps(persons, cls=PersonEncoder)


app.run()
