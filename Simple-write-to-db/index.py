from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
import json

# Configure access to db
POSTGRES_URL = "127.0.0.1:5432"
POSTGRES_USER = "postgres"
POSTGRES_PW = "Zoopark"
POSTGRES_DB = "First"

# Connection string
DB_URL = 'postgresql://{user}:{pw}@{url}/{db}'.format(user=POSTGRES_USER, pw=POSTGRES_PW,
                                                      url=POSTGRES_URL, db=POSTGRES_DB)

app = Flask(__name__)

# App config
app.config['SQLALCHEMY_DATABASE_URI'] = DB_URL
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# Connect app and db
db = SQLAlchemy(app)


# Data model for table in db
class Person(db.Model):

    # Columns
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(15))
    surname = db.Column(db.String(20))

    def __init__(self, name, surname):
        self.name = name
        self.surname = surname

    def __repr__(self):
        return '<Person %>' % self.name


# Make Person JSON_Serializable
class PersonEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Person):
            return {'name': obj.name, 'surname': obj.surname}
        return json.JSONEncoder.default(self, obj)


@app.route('/', methods=['GET', 'POST'])
def write_to_db():
    if request.method == 'POST':
        data = json.loads(request.data)
        name = data['name']
        surname = data['surname']

        new_person = Person(name, surname)

        db.session.add(new_person)
        db.session.commit()
        res = json.dumps(new_person, cls=PersonEncoder)
        return res
    return "something wrong"


app.run()
