from flask import Flask, request, send_file
from flask_mail import Mail, Message
from flask_sqlalchemy import SQLAlchemy
from threading import Thread
import random
import string

app = Flask(__name__)

app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USE_SSL'] = True
app.config['MAIL_USERNAME'] = 'Your_gmail'
app.config['MAIL_PASSWORD'] = 'Your_gmail_password'

mail = Mail(app)


POSTGRES_URL = "127.0.0.1:5432"
POSTGRES_USER = "postgres"
POSTGRES_PW = "Zoopark"
POSTGRES_DB = "First"
DB_URL = 'postgresql://{user}:{pw}@{url}/{db}'.format(user=POSTGRES_USER, pw=POSTGRES_PW,
                                                      url=POSTGRES_URL, db=POSTGRES_DB)

app.config['SQLALCHEMY_DATABASE_URI'] = DB_URL
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


chars = string.digits+string.ascii_uppercase
rnd = random.SystemRandom()


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(50))
    approved = db.Column(db.Boolean)
    salt = db.Column(db.String(50))

    def __init__(self, email):
        self.email = email
        self.approved = False
        self.salt = ''.join(rnd.choices(chars, k=50))


def send_async_email(app, msg):
    with app.app_context():
        mail.send(msg)


@app.route('/register', methods=['GET', 'POST'])
def send_message():
    if request.method == 'POST':
        email = request.form['email']
        user = db.session.query(User).filter_by(email=email).first()

        if user is None:
            new_user = User(email)
            personal_url = 'http://localhost:5000/complete/{0}'.format(new_user.salt)
            letter = """
            <h1>Thanks for registering your account!</h1>
            <br>
            <p>Hello, to complete registration you need to go to the link below</p>
            <a href={0}>Finish registration!</a>
            """.format(personal_url)

            db.session.add(new_user)
            db.session.commit()

            msg = Message('Registration', sender='damirhnv@gmail.com', recipients=[email], html=letter)
            thr = Thread(target=send_async_email, args=[app, msg])
            thr.start()

            return "Please, check your email"
        else:
            return "Bro, you'r already registered"
    else:
        return send_file('register.html')


@app.route('/complete/<personal_url>')
def finish_register(personal_url):
    u = db.get_tables_for_bind()[0]
    up = u.update().where(u.c.salt == personal_url).values(approved=True)
    db.session.execute(up)
    db.session.commit()
    return """
    <h1>Thanks for registering</h1>
    <a href="http://127.0.0.1:5000/?email={0}">Go to content</a>
    """.format(db.session.query(User).filter_by(salt=personal_url).first().email)


@app.route('/')
def view_content():
    email = request.query_string.decode('utf-8')[6:]
    user = db.session.query(User).filter_by(email=email).first()
    if user is None:
        return send_file('register.html')
    elif user.approved:
        return send_file('content.html')
    else:
        return "Please, check your email"
    pass


app.run()
