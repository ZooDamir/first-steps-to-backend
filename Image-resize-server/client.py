import requests
import json
import cv2

addr = 'http://localhost:5000'
test_url = addr + '/test'

# prepare headers for http request
content_type = 'image/jpeg'
headers = {
    'content-type': content_type,
    'login': 'SomeLogin'
}


def post_image(img_file):
    """ post image and return the response """
    img = open(img_file, 'rb').read()
    response = requests.post(test_url, data=img, headers=headers)
    return response.text


k = 'c'
while k != 'q':
    print(post_image('I:/SteamLibrary/steamapps/common/The Witcher 3/ARTWORK/The_Witcher_3_Wild_Hunt_Port.jpg'))
    k = input('Press any key to make request ')

