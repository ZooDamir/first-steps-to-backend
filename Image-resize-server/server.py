from flask import Flask, request
from PIL import Image

app = Flask(__name__)


@app.route('/test', methods=['POST'])
def receive_image():
    data = request.files['pic'].read()  # image encoded in bytes
    content_type = request.files['pic'].content_type.split('/')[1]
    login = 'TEST'  # request.headers['login']
    original_image_name = 'Original'+login

    # save original image
    temp = open(original_image_name, 'wb')
    temp.write(data)
    temp.close()

    # save mini image
    size = (64, 64)
    mini_name = 'mini'+login
    save_image(original_image_name, mini_name, size, content_type)

    # save avatar image
    size = (256, 256)
    avatar_name = 'avatar'+login
    save_image(original_image_name, avatar_name, size, content_type)

    return {
        'original_image': original_image_name,
        'mini_image': mini_name,
        'avatar_image': avatar_name
    }


def save_image(source_image, output_image_name, size, content_type):
    im = Image.open(source_image)
    im.thumbnail(size, Image.ANTIALIAS)
    im.save(output_image_name, content_type)


app.run()
