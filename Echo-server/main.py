from flask import Flask, request
from datetime import datetime

app = Flask(__name__, static_folder='')


@app.route('/')
def index():
    return app.send_static_file('index.html')


@app.route('/First', methods=['POST', 'GET'])
def check():
    if request.method == "POST":
        time = datetime.now()
        current = datetime.now()
        while (current - time).seconds < 3:
            current = datetime.now()
        return request.data
    return "<h1>Send POST request</h1>"


app.run(port=80)
